import discord
from discord.ext import commands
from core.classes import *
import json
import pymongo
import dns

client = pymongo.MongoClient(
    "mongodb+srv://Kay:Kay20030910@kaybotpython-l3ixd.gcp.mongodb.net/test?retryWrites=true&w=majority")
leveldata = client['KayBotPython']['level']


class Level(Cog_Extension):
    @commands.Cog.listener()
    async def on_message(self, message):
        if not message.author.bot:
            if leveldata.find_one({"_id": str(message.author.id)}) == None:
                insert = {}
                insert['_id'] = str(message.author.id)
                insert['challengepass'] = 0
                insert['coin'] = 1
                insert['level'] = 1
                insert['xp'] = 1
                leveldata.insert_one(insert)
            else:
                results = leveldata.find_one({"_id": str(message.author.id)})
                results['xp'] += 1
                if results['xp'] >= results['level'] * 3:
                    results['level'] += 1
                    results['xp'] = 0
                    results['coin'] += results['level']
                    leveldata.update_one({"_id": str(message.author.id)}, {
                        "$set": {"level": results['level'], "xp": results['xp'], "coin": results['coin']}})
                    await message.channel.send(f"恭喜{message.author.mention}！您升級到了等級{results['level']}")
                else:
                    leveldata.update_one({"_id": str(message.author.id)}, {
                        "$set": {"xp": results['xp']}})


def setup(bot):
    bot.add_cog(Level(bot))
