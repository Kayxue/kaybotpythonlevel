import discord
import json
import pymongo
import dns
from discord.ext import commands
from core.classes import *

with open("JSON/setting.json", mode="r", encoding="utf8")as jfile:
    jdata = json.load(jfile)

client = pymongo.MongoClient(
    jdata['MongoDBWebsite'])
leveldataold = client['KayBotPython']['level']


class LevelAndCurrency(Cog_Extension):
    @commands.command()
    async def rank(self, ctx, mem: discord.Member = None):
        if mem == None:
            leveldata = leveldataold.find_one({"_id": str(ctx.author.id)})
            if not leveldata == None:
                embed1 = discord.Embed(
                    title="您的等級卡", description=f"{ctx.author.name}的等級卡", color=self.normalembedcolor)
                embed1.add_field(
                    name="目前等級", value=f"{leveldata['level']}")
                embed1.add_field(
                    name="目前XP", value=f"{int(leveldata['xp'])}/{int(leveldata['level'])*3}")
                embed1.add_field(
                    name="目前擁有之金幣：", value=f"{leveldata['coin']}")
                await ctx.channel.send(embed=embed1)
            else:
                await ctx.channel.send("請先聊天喔！")
        else:
            if not mem.bot:
                leveldata = leveldataold.find_one({"_id": str(mem.id)})
                if not leveldata == None:
                    embed1 = discord.Embed(
                        title=f"使用者「{mem}」的資訊卡", description="您正在查看別人的等級卡喔！", color=self.normalembedcolor)
                    embed1.add_field(
                        name="目前等級", value=f"{leveldata['level']}")
                    embed1.add_field(
                        name="目前XP", value=f"{int(leveldata['xp'])}/{int(leveldata['level'])*3}")
                    embed1.add_field(
                        name="目前擁有之金幣：", value=f"{leveldata['coin']}")
                    await ctx.channel.send(embed=embed1)
                else:
                    await ctx.channel.send("您指定之使用者尚未產生等級資料喔！")
            else:
                await ctx.channel.send("機器人沒有等級卡喔！")


def setup(bot):
    bot.add_cog(LevelAndCurrency(bot))
