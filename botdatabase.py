import discord
import random
import json
import os
import asyncio
from discord.ext import commands
from core.classes import *
from disputils import *

with open('JSON/setting.json', 'r', encoding='utf8') as jfile:
    jdata = json.load(jfile)


class Bot(commands.Bot):
    def __init__(self):
        super(Bot, self).__init__(command_prefix=[
            's!', 'sesame '], help_command=None)
        self.add_cog(maintask(self))

    async def on_ready(self):
        print('BotDataBase loading finished!')


class maintask(Cog_Extension):

    def is_it_me(ctx):
        return ctx.author.id == 470516498050580480

    @commands.command(pass_context=True)
    @commands.check(is_it_me)
    async def loaddb(self, ctx, extension):
        bot.load_extension(f'database.{extension}')
        embed1 = discord.Embed(
            title='指定類別指令載入成功！', description=f'成功載入{extension}類別指令！', color=0xc8f4fb)
        await ctx.send(embed=embed1)

    @loaddb.error
    async def loaddb_error(self, ctx, error):
        if isinstance(error, discord.ext.commands.CheckFailure):
            embed1 = discord.Embed(
                title='權限不足！', description='只有湯圓本人能執行此指令喔！', color=0xc8f4fb)
            await ctx.channel.send(embed=embed1)
        elif isinstance(error, discord.ext.commands.MissingRequiredArgument):
            embed1 = discord.Embed(
                title='請輸入要載入之類別！', description='不會用嗎？沒關係，我幫你', color=0xc8f4fb)
            embed1.add_field(name='用法', value='``s!loaddb [類別名稱]``')
            embed1.add_field(name='範例', value='``s!loaddb event``')
            await ctx.channel.send(embed=embed1)
        else:
            embed1 = discord.Embed(
                title='載入失敗！', description='對不起，無法載入該類別之指令', color=0xc8f4fb)
            embed1.add_field(name='錯誤碼', value=error, inline=False)
            await ctx.channel.send(embed=embed1)

    @commands.command()
    @commands.check(is_it_me)
    async def unloaddb(self, ctx, extension):
        bot.unload_extension(f'database.{extension}')
        embed1 = discord.Embed(
            title='指定類別指令解除載入成功！', description=f'成功解除載入{extension}類別指令！', color=0xc8f4fb)
        await ctx.send(embed=embed1)

    @unloaddb.error
    async def unloaddbd_error(self, ctx, error):
        if isinstance(error, discord.ext.commands.CheckFailure):
            embed1 = discord.Embed(
                title='權限不足！', description='只有湯圓本人能執行此指令喔！', color=0xc8f4fb)
            await ctx.channel.send(embed=embed1)
        elif isinstance(error, discord.ext.commands.MissingRequiredArgument):
            embed1 = discord.Embed(title='請輸入要解除載入之類別！',
                                   description='不會用嗎？沒關係，我幫你', color=0xc8f4fb)
            embed1.add_field(name='用法', value='``s!unloaddb [類別名稱]``')
            embed1.add_field(name='範例', value='``s!unloaddb event``')
            await ctx.channel.send(embed=embed1)
        else:
            embed1 = discord.Embed(
                title='解除載入失敗！', description='對不起，無法解除載入該類別之指令', color=0xc8f4fb)
            embed1.add_field(name='錯誤碼', value=error, inline=False)
            await ctx.channel.send(embed=embed1)

    @commands.command()
    @commands.check(is_it_me)
    async def reloaddb(self, ctx, extension):
        bot.reload_extension(f'database.{extension}')
        embed1 = discord.Embed(
            title='已重新載入指定類別指令！', description=f'成功重新載入{extension}類別指令！', color=0xc8f4fb)
        await ctx.send(embed=embed1)

    @reloaddb.error
    async def reloaddb_error(self, ctx, error):
        if isinstance(error, discord.ext.commands.CheckFailure):
            embed1 = discord.Embed(
                title='權限不足！', description='只有湯圓本人能執行此指令喔！', color=0xc8f4fb)
            await ctx.channel.send(embed=embed1)
        elif isinstance(error, discord.ext.commands.MissingRequiredArgument):
            embed1 = discord.Embed(title='請輸入要重新載入之類別！',
                                   description='不會用嗎？沒關係，我幫你', color=0xc8f4fb)
            embed1.add_field(name='用法', value='``s!reloaddb [類別名稱]``')
            embed1.add_field(name='範例', value='``s!reloaddb event``')
            await ctx.channel.send(embed=embed1)
        else:
            embed1 = discord.Embed(
                title='重新載入失敗！', description='對不起，不明原因，無法重新載入該類別之指令', color=0xc8f4fb)
            embed1.add_field(name='錯誤碼', value=error, inline=False)
            await ctx.channel.send(embed=embed1)


bot = Bot()

for filename in os.listdir('./database'):
    if filename.endswith('.py'):
        bot.load_extension(f'database.{filename[:-3]}')

if __name__ == "__main__":
    bot.run(jdata["TOKEN"])
